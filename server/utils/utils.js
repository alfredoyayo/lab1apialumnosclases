const InvalidArgumentException = require('./../exceptions/InvalidArgumentException');

const pick = (object,keys)=>{
    const _object = {};
    for (const key of keys) {
        if(key in object){
            _object[key]=object[key]
        }
    }
    return _object;
} 

const verifyEmpty = (object)=>{ 
    for (const key in object) {
        const value=object[key];
        if(!value){
            throw new InvalidArgumentException(`El campo ${key} es requerido o vacío`);
            }
        
        if(typeof(value)==="string"&&value.length<=0){
            throw new InvalidArgumentException(`El campo ${key} no debe estar vacío`);
        }
    } 
    return object;
}

const verifyEmail = (value)=>{
    verifyEmpty(value,"El campo email esta vacío");
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!regex.test(String(value).toLowerCase())){
        throw new InvalidArgumentException("el email no es válido");
    }
    return value;
}

module.exports = {
    pick,
    verifyEmail,
    verifyEmpty
}