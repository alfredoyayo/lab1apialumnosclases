require('./config/config');
const express = require('express');
const app = express();

app.use(require('./routes'));

app.listen(process.env.PORT,()=>{
    console.log("corriendo en el puerto "+process.env.PORT);
});