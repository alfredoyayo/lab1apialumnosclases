const InvalidArgumentException = require('./../exceptions/InvalidArgumentException');

const errorHandling =(error, req, res, next) => {
    if (error instanceof InvalidArgumentException) {
        res.status(400).send({error:error.message});
    } else {
        res.status(500).send({error:error.message});
    }
}

module.exports = errorHandling;