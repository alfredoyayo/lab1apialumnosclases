const course = require('./../data/course');
const utils = require('./../utils/utils'); 

const getAll=(_, res)=>{
    res.send(course.getAll());
};

const saveCourse=(req,res)=> {
    const {name,teacher,level} = req.body;
    const newCourse = {name,teacher,level}; 
    const data = course.save(newCourse);
    res.status(201).send({data});
};

const updateCourse=(req,res)=> {
    const {id} = req.params;  
    const body = utils.pick(req.body,['name','teacher','level']);
    const data= course.findByIdAndUpdate(id,body);
    res.send({data});
};

module.exports = {
    getAll,
    saveCourse,
    updateCourse
}