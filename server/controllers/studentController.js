const student = require('./../data/student');
const utils = require('./../utils/utils'); 

const getStudents=(_, res)=>{
    res.send(student.getAll());
};

const saveStudent=(req,res)=> {
    const {names,surnames,email} = req.body;
    const newStudent = {names,surnames,email}; 
    const data = student.save(newStudent);
    res.status(201).send({data});
};

const updateStudent=(req,res)=> {
    const {id} = req.params;  
    const body = utils.pick(req.body,['names','surnames','email']);
    const data= student.findByIdAndUpdate(id,body);
    res.send({data});
};

module.exports = {
    getStudents,
    saveStudent,
    updateStudent
}
