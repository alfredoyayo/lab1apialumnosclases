'use strict';

module.exports = class InvalidArgumentException extends Error {
  constructor(message = "", ...args) {
    super(message, ...args);
  }
}