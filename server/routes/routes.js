const BASE_URL = '/api'; 

const studentsPATH = '/students';
const coursesPATH = '/courses'

module.exports = {BASE_URL, studentsPATH, coursesPATH}