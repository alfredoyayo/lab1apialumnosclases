const express = require('express');
const app = express();
const errorHandling = require('../middlewares/errorHandling');

app.use(express.json());
app.use(require('./students'));
app.use(require('./courses'));
app.use(errorHandling);

module.exports = app;