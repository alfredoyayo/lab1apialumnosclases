const express = require('express');
const app = express();
const courses=require('../controllers/courseController');
const {BASE_URL, coursesPATH } = require('./routes');

app.get(`${BASE_URL}${coursesPATH}`,courses.getAll);
app.post(`${BASE_URL}${coursesPATH}`,courses.saveCourse); 
app.patch(`${BASE_URL}${coursesPATH}/:id`,courses.updateCourse); 

module.exports = app

