const express = require('express');
const app = express();
const student=require('../controllers/studentController');
const {BASE_URL, studentsPATH } = require('./routes');

app.get(`${BASE_URL}${studentsPATH}`,student.getStudents);

app.post(`${BASE_URL}${studentsPATH}`,student.saveStudent);

app.patch(`${BASE_URL}${studentsPATH}/:id`,student.updateStudent); 

module.exports = app
