const utils = require('./../utils/utils');
const InvalidArgumentException = require('./../exceptions/InvalidArgumentException');

const students = [
    {id:1,names : "Fredo",surnames : "Ortiz Mitma",email: "fredo@gmai.com"},
    {id:2,names : "Jose",surnames : "Perez Valle",email: "jose@gmai.com"},
    {id:3,names : "Ismael",surnames : "Riojas Barrios ",email: "ismael@gmai.com"}
];

const getAll = ()=> students;

const save = (data)=> { 
    validateFields(data);
    data.id = getAll().length +1;
    students.push(data);
    return data;
};

const findByIdOrFail=(id)=>{
    const object = getAll().find((object)=> object.id === parseInt(id));  
    if(!object){
        throw new InvalidArgumentException(`no se pudo encontrar un objeto con el id: ${id}`);
    }
    return object;
}

const update = (data)=> { 
    const index = getAll().findIndex((object)=>object.id === parseInt(data.id));  
    const _newData = {...getAll()[index] , ...data};
    getAll()[index]=_newData;
    return _newData;
};

const findByIdAndUpdate = (id,data)=> { 
    validateFields(data);
    const _object=findByIdOrFail(id);  
    return update({..._object,...data});
};

const validateFields =(data)=>{
    utils.verifyEmpty(data);
    if(data?.email){
        utils.verifyEmail(data.email);
    }
}
module.exports={
    getAll,
    save,
    update,
    findByIdOrFail,
    findByIdAndUpdate
}