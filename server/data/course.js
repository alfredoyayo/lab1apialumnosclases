const utils = require('./../utils/utils');
const InvalidArgumentException = require('./../exceptions/InvalidArgumentException');

const courses = [
    {id:1,name : "Ios",teacher : "Kenyi Rodriguez",level: "Avanzado"},
    {id:2,name : "Node",teacher : "Ismael Malca",level: "Avanzado"},
    {id:3,name : "Android",teacher : "Juan Jose Ledesma",level: "Avanzado"}
];

const getAll = ()=> courses;

const save = (data)=> { 
    validateFields(data);
    data.id = getAll().length +1;
    courses.push(data);
    return data;
};

const findByIdOrFail=(id)=>{
    const object = getAll().find((object)=> object.id === parseInt(id));  
    if(!object){
        throw new InvalidArgumentException(`no se pudo encontrar un objeto con el id: ${id}`);
    }
    return object;
}

const update = (data)=> { 
    const index = getAll().findIndex((object)=>object.id === parseInt(data.id));  
    const _newData = {...getAll()[index] , ...data};
    getAll()[index]=_newData;
    return _newData;
};

const findByIdAndUpdate = (id,data)=> { 
    validateFields(data);
    const _object=findByIdOrFail(id);  
    return update({..._object,...data});
};

const validateFields =(data)=>{
    utils.verifyEmpty(data); 
}
module.exports={
    getAll,
    save,
    update,
    findByIdOrFail,
    findByIdAndUpdate
}